﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.XR;

namespace VB
{
    public class PhotoSphere : MonoBehaviour
    {
        private string m_PhotoURI;
        private UnityPlugin m_Plugin;
        private event Action<Vector3> m_OnRotateEvent;
        private const float DRAG_RATE = .2f;
        private float m_DragYawDegrees;
        private float m_DragPitchDegrees;
        [SerializeField]
        private Camera m_Camera;
        private bool m_PhotoLoaded;

        /*! < Event when SDK is initialized. Most SDK API should only be invoked after receiving this event. */
        public event Action SDKInitializedEvent
        {
            add { GetPlugin().SDKInitializationDoneEvent += value; }
            remove { GetPlugin().SDKInitializationDoneEvent -= value; }
        }

        /*! < Event when SDK initialized failed.*/
        public event Action<VbErrorType, String> SDKErrorEvent
        {
            add { GetPlugin().VbErrorEvent += value; }
            remove { GetPlugin().VbErrorEvent -= value; }
        }

        /*! < Event when photo succeeded.*/
        public event Action<string> OnLoadSucceedEvent
        {
            add { GetPlugin().OnPhotoLoadSucceedEvent += value; }
            remove { GetPlugin().OnPhotoLoadSucceedEvent -= value; }
        }

        public event Action<string, VbErrorType> OnLoadFailedEvent
        {
            add { GetPlugin().OnPhotoLoadFailedEvent += value; }
            remove { GetPlugin().OnPhotoLoadFailedEvent -= value; }
        }

        public event Action<Vector3> OnRotateEvent
        {
            add { m_OnRotateEvent += value; }
            remove { m_OnRotateEvent -= value; }
        }

        public bool StereoEnabled
        {
            get { return GetPlugin().IsStereoEnabled(); }
            set { GetPlugin().SetStereoEnabled(value); }
        }

        public bool GyroEnabled
        {
            get { return Input.gyro.enabled; }
            set { Input.gyro.enabled = value; }
        }

        public void Render(string photoURI)
        {
            ResetCameras();
            m_PhotoURI = photoURI;
            m_Plugin.LoadPhoto(photoURI);
        }

        public void SetVrModeEnabled(bool enabled)
        {
            // Only affect cardboard platform
            if (String.Compare(XRSettings.loadedDeviceName, "cardboard", true) == 0
                || String.Compare(XRSettings.loadedDeviceName, "", true) == 0)
            {
                if (XRSettings.enabled != enabled)
                {
                    if (enabled)
                    {
                        StartCoroutine(SwitchToVR());
                    }
                    else
                    {
                        StartCoroutine(SwitchTo2D());
                    }
                }
            }
        }

        public bool IsVrModeEnabled()
        {
            // Always return false if loadedDeviceName is empty
            if (String.Compare(XRSettings.loadedDeviceName, "", true) == 0)
            {
                return false;
            }
            // Always return true if platform is oculus
            if (String.Compare(XRSettings.loadedDeviceName, "oculus", true) == 0)
            {
                return true;
            }
            // Alwasy return true if platform is viveport
            if (String.Compare(XRSettings.loadedDeviceName, "MockHMD", true) == 0)
            {
                return true;
            }
            return XRSettings.enabled;
        }

        UnityPlugin GetPlugin()
        {
            if (m_Plugin == null)
            {
                m_Plugin = UnityPlugin.Instance();
            }
            return m_Plugin;
        }

        // Use this for initialization
        void Start()
        {
            m_Plugin = GetPlugin();
            m_Plugin.InitPlayer();
            m_Plugin.AttachSphere(gameObject, null);
            m_Plugin.AttachCamera(m_Camera);
#if VB_VIVE_WAVE_SDK
            m_Plugin.AttachHead(m_Camera.GetComponentInChildren<WaveVR_DevicePoseTracker>().gameObject);
#endif
            m_DragYawDegrees = 0f;
            m_DragPitchDegrees = 0f;
        }

        // Update is called once per frame
        void Update()
        {
            m_Plugin.Update();
            if (m_PhotoLoaded)
            {
                m_Plugin.UpdateTexture();
            }
            UpdateRotate();
        }

        void OnEnable()
        {
            GetPlugin().OnPhotoLoadSucceedEvent += OnPhotoLoadSucceed;
        }

        void OnDisable()
        {
            GetPlugin().OnPhotoLoadSucceedEvent -= OnPhotoLoadSucceed;
        }

        void OnDestroy()
        {
            m_Plugin.AttachSphere(null, null);
            m_Plugin.AttachCamera(null);
            m_Plugin.AttachHead(null);
        }

        void OnPhotoLoadSucceed(string videoId)
        {
            m_PhotoLoaded = true;
        }

        // Call via `StartCoroutine(SwitchToVR())` from your code. Or, use
        // `yield SwitchToVR()` if calling from inside another coroutine.
        IEnumerator SwitchToVR()
        {
            // Device names are lowercase, as returned by `XRSettings.supportedDevices`.
            string desiredDevice = "cardboard";
            // Some VR Devices do not support reloading when already active, see
            // https://docs.unity3d.com/ScriptReference/XR.XRSettings.LoadDeviceByName.html
            if (String.Compare(XRSettings.loadedDeviceName, desiredDevice, true) != 0)
            {
                XRSettings.LoadDeviceByName(desiredDevice);
                // Must wait one frame after calling `XRSettings.LoadDeviceByName()`.
                yield return null;
            }
            // Now it's ok to enable VR mode.
            XRSettings.enabled = true;
        }

        // Call via `StartCoroutine(SwitchTo2D())` from your code. Or, use
        // `yield SwitchTo2D()` if calling from inside another coroutine.
        IEnumerator SwitchTo2D()
        {
            // Empty string loads the "None" device.
            XRSettings.LoadDeviceByName("");
            // Must wait one frame after calling `XRSettings.LoadDeviceByName()`.
            yield return null;
            // Not needed, since loading the None (`""`) device takes care of this.
            // XRSettings.enabled = false;
            // Restore 2D camera settings.
            ResetCameras();
            ResetPhotoSphere();
        }

        // Resets camera transform and settings on all enabled eye cameras.
        void ResetCameras()
        {
            if (m_Camera != null)
            {
                if (m_Camera.enabled && m_Camera.stereoTargetEye != StereoTargetEyeMask.None)
                {
                    // Reset local position.
                    // Only required if you change the camera's local position while in 2D mode.
                    m_Camera.transform.localPosition = Vector3.zero;

                    // Reset local rotation.
                    // Only required if you change the camera's local rotation while in 2D mode.
                    m_Camera.transform.localRotation = Quaternion.identity;
                }
            }
        }

        void ResetPhotoSphere()
        {
            transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
        }

        void UpdateRotate()
        {
            if (XRSettings.enabled)
            {
                // Unity takes care of updating camera transform in VR.
                return;
            }
            if (m_Camera != null)
            {
                if (Input.gyro.enabled)
                {
                    m_Camera.transform.localRotation = Quaternion.Euler(90f, 0f, 0f) *
                          Input.gyro.attitude * Quaternion.Euler(0f, 0f, 180f);
                }
                else
                {
                    CheckDrag();
                    m_Camera.transform.localRotation = Quaternion.Euler(m_DragPitchDegrees, m_DragYawDegrees, 0f);
                }
                if (m_OnRotateEvent != null)
                {
                    m_OnRotateEvent(m_Camera.transform.localRotation.eulerAngles);
                }
            }
        }

        void CheckDrag()
        {
            if (Input.touchCount != 1)
            {
                return;
            }
            Touch touch = Input.GetTouch(0);
            if (touch.phase != TouchPhase.Moved)
            {
                return;
            }
            m_DragYawDegrees += touch.deltaPosition.x * DRAG_RATE;
            m_DragPitchDegrees += touch.deltaPosition.y * DRAG_RATE;
        }

        void OnApplicationPause(bool pauseStatus)
        {
            GetPlugin().OnApplicationPause(pauseStatus);
        }
    }
}

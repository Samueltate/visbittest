﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace VB {

[InitializeOnLoad]
public static class VbLayerCreator {

    public enum Layer {
      LeftLayer = 8,
      RightLayer = LeftLayer + 1,
    };

    static VbLayerCreator(){
      CreateLayer();
    }

    static void CreateLayer()
    {
      SerializedObject tagManager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

      SerializedProperty layers = tagManager.FindProperty("layers");
      if (layers == null || !layers.isArray)
      {
        return;
      }

      string[] layerNames = new string[2];
      layerNames[0] = "LeftEyeLayer";
      layerNames[1] = "RightEyeLayer";

      int[] layerNumbers = new int[2];
            layerNumbers[0] = 8;//(int)UnityPluginInternal.Layer.LeftLayer;
            layerNumbers[1] = 9;//(int)UnityPluginInternal.Layer.RightLayer;
      for (int i = 0; i < 2; i++)
      {
        SerializedProperty layerSP = layers.GetArrayElementAtIndex(layerNumbers[i]);
        if (layerSP.stringValue != layerNames[i])
        {
          layerSP.stringValue = layerNames[i];
        }
      }

      tagManager.ApplyModifiedProperties();
    }
  }

}
﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using VB.UnityEditor.iOS.Xcode;
using System.IO;

public class PostProcessXCodeProject
{

	[PostProcessBuild]
	public static void ChangeXcodePlist (BuildTarget buildTarget, string pathToBuiltProject)
	{

		if (buildTarget == BuildTarget.iOS) {

			// Get plist
			string plistPath = pathToBuiltProject + "/Info.plist";
			PlistDocument plist = new PlistDocument ();
			plist.ReadFromString (File.ReadAllText (plistPath));

			// Get root
			PlistElementDict rootDict = plist.root;

			// Change value of CFBundleVersion in Xcode plist
			var buildKey = "co.visbit.vrplayer.appIdentifier";
			#warning Change the "YOUR_APP_ID" below with your real app ID
			rootDict.SetString (buildKey, "visbit_dev"); 
			rootDict.SetString ("co.visbit.vrplayer.sailfishServer", "dev"); 	  
			// Write to file
			File.WriteAllText (plistPath, plist.WriteToString ());
		}
	}

	[PostProcessBuild]
	static void ChangeXcodeProject (BuildTarget target, string pathToBuiltProject)
	{
		if (target == BuildTarget.iOS) {
			string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject ();
			proj.ReadFromString (File.ReadAllText (projPath));

			string unityTarget = proj.TargetGuidByName ("Unity-iPhone");
			proj.SetBuildProperty (unityTarget, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "YES");
			proj.SetBuildProperty (unityTarget, "LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");

			// Add the run script build phase
			proj.AddRunScriptBuildPhaseToProject (unityTarget, "Embed Visbit Streaming Frameworks", "\"${SRCROOT}/Copy-frameworks.sh\"");

			proj.WriteToFile (projPath);

			//Copy the script to the project
			string source = Application.dataPath + "/Plugins/iOS/VisbitVRPlayer/Copy-frameworks.sh";
			File.Copy (source, pathToBuiltProject + "/Copy-frameworks.sh", true);

		}
	}

}
﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using Visbit;

public class VideoDemo : MonoBehaviour
{
    [SerializeField]
    private Text mssageView;

    [SerializeField]
    private VideoPlayer videoPlayer;

    [SerializeField]
    private string videoURL;

    private VisbitWebAPIClient webAPIClient;

    private VisbitDownloadRequest downloadRequest;

    private bool isLoading = false;

    private void Awake()
    {
        webAPIClient = VisbitWebAPIClient.Instance;
    }

    void Start()
    {
        Debug.LogFormat("[VB_SDK] Eyebuffer size {0} x {1}", XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight);
        OVRPlugin.systemDisplayFrequency = 60.0f;

        if (mssageView != null)
        {
            mssageView.enabled = true;
            mssageView.color = Color.white;
            mssageView.text = "Welcome to Visbit VRPlayer";
        }

        webAPIClient.Register();
    }

    void OnEnable()
    {
        webAPIClient.RegistrationSuccessEvent += OnRegistrationSuccess;
        webAPIClient.RegistrationFailureEvent += OnRegistrationFailure;
    }

    void OnDisable()
    {
        webAPIClient.RegistrationSuccessEvent -= OnRegistrationSuccess;
        webAPIClient.RegistrationFailureEvent -= OnRegistrationFailure;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void OnRegistrationSuccess()
    {
        if (mssageView != null)
        {
            mssageView.enabled = true;
            mssageView.color = Color.white;
            mssageView.text = "WebAPI Registered";
        }

        if (string.IsNullOrEmpty(videoURL))
        {
            return;
        }
        var uri = new Uri(videoURL);
        if (uri.Scheme == "http" || uri.Scheme == "https")
        {
            StartCoroutine(Download(uri));
        }
        else if (File.Exists(uri.AbsolutePath))
        {
            if (!isLoading)
            {
                isLoading = true;
                videoPlayer.Load(uri);
            }
        }
        else if (mssageView != null)
        {
            mssageView.color = Color.red;
            mssageView.text = "File doesn't exist at " + uri.AbsolutePath;
            Debug.LogErrorFormat("File doesn't exist at {0}", uri.AbsolutePath);
        }
    }

    void OnRegistrationFailure(string errorMessage)
    {
        if (mssageView != null)
        {
            mssageView.enabled = true;
            mssageView.color = Color.red;
            mssageView.text = errorMessage;
        }
    }

    #region Player Control

    public void Play()
    {
        if (videoPlayer != null)
        {
            videoPlayer.Play();
        }
    }

    public void Pause()
    {
        if (videoPlayer != null)
        {
            videoPlayer.Pause();
        }
    }

    public void Rewind()
    {
        if (videoPlayer != null)
        {
            var playtime = Math.Max(0, videoPlayer.time - 30000);
            videoPlayer.SeekTo(playtime);
        }
    }

    public void Forward()
    {
        if (videoPlayer != null)
        {
            var playtime = videoPlayer.time + 30000;
            if (playtime >= videoPlayer.length) { playtime = 0; }
            videoPlayer.SeekTo(playtime);
        }
    }


    #endregion

    #region Sample for Downloading video from customized endpoint

    private IEnumerator Download(Uri url)
    {
        downloadRequest = webAPIClient.DownloadVideo(url);
        downloadRequest.SendHTTPRequest();

        while (!downloadRequest.isDone && !downloadRequest.isNetworkError)
        {
            if (mssageView != null)
            {
                mssageView.enabled = true;
                mssageView.color = Color.white;
                mssageView.text = "Downloading " + downloadRequest.downloadProgress * 100 + "%";
            }
            yield return null;
        }

        if (downloadRequest.isDone)
        {
            if (!isLoading)
            {
                isLoading = true;
                videoPlayer.Load(new Uri(downloadRequest.destPath));
            }
        }
        else if (downloadRequest.isNetworkError)
        {
            if (mssageView != null)
            {
                mssageView.enabled = true;
                mssageView.color = Color.red;
                mssageView.text = downloadRequest.error;
            }
        }

    }

    #endregion

}


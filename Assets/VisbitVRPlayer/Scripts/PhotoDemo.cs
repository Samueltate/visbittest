﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.UI;

namespace VB
{
    public class PhotoDemo : MonoBehaviour
    {
        [SerializeField]
        Text m_MessageView;

        [SerializeField]
        PhotoSphere m_PhotoSphere;

        [SerializeField]
        bool m_IsSingleEye = false;

        [SerializeField]
        bool m_ReadFromLocal = false;

        UnityPlugin m_Plugin;

        string EndPoint = "https://ap-northeast-1-vp0.vixst.com/photos/";

        string[] m_PhotoIDs = {
            // stereo
            "5xq4t2z83n",
            "bCwY63Xy6O",

            // mono
            "uFszzYGjQk/",
            "xWLUNrN3N9/",
            "uY50bbXvtV/",
            "GsmC9OLOI8/",
            "WKhp6I7ehi/",
        };
        int m_CurrentIndex;
        bool m_IsInitialized;
        bool m_IsPhotoLoaded;
        int m_TapCount = 0;
        float m_DoubleTapTimer = 0.0f;

        void Awake()
        {
            if (m_ReadFromLocal)
            {
#if UNITY_ANDROID
                EndPoint = "file://" + Application.persistentDataPath + "/PhotoSDK/";
#elif UNITY_IOS
                // On iOS, please place the file under Documents(Application.persistentDataPath) folder"
                EndPoint = "file://" + Application.persistentDataPath + "/PhotoSDK/";
#endif
            }
        }

        void Start()
        {
#if UNITY_ANDROID
            if (String.Compare(XRSettings.loadedDeviceName, "oculus", true) == 0) {
                OVRPlugin.cpuLevel = 2;
                OVRPlugin.gpuLevel = 3;
                if (OVRPlugin.productName.Equals("Oculus Go")) {
                    OVRPlugin.systemDisplayFrequency = 72.0f;
                }
                XRSettings.eyeTextureResolutionScale = 1440.0f / XRSettings.eyeTextureHeight;
            }
            else
            {
                XRSettings.eyeTextureResolutionScale = 1600.0f / XRSettings.eyeTextureHeight;
            }
#elif UNITY_IOS
            XRSettings.eyeTextureResolutionScale = 1.0f;
#endif
            Debug.LogFormat("Eyebuffer size {0} x {1}", XRSettings.eyeTextureWidth, XRSettings.eyeTextureHeight);

            m_CurrentIndex = 0;
            if (String.Compare(XRSettings.loadedDeviceName, "cardboard", true) == 0 && m_IsSingleEye)
            {
                SwitchVRto2D();
            }
        }

        void OnEnable()
        {
            m_PhotoSphere.SDKInitializedEvent += OnSDKInitialized;
            m_PhotoSphere.SDKErrorEvent += OnSDKError;
            m_PhotoSphere.OnLoadSucceedEvent += OnPhotoLoaded;
            m_PhotoSphere.OnLoadFailedEvent += OnPhotoLoadFailed;
        }

        void OnDisable()
        {
            m_PhotoSphere.SDKInitializedEvent -= OnSDKInitialized;
            m_PhotoSphere.SDKErrorEvent -= OnSDKError;
            m_PhotoSphere.OnLoadSucceedEvent -= OnPhotoLoaded;
            m_PhotoSphere.OnLoadFailedEvent -= OnPhotoLoadFailed;
        }

        void OnSDKInitialized()
        {
            m_IsInitialized = true;
            m_MessageView.enabled = true;
            m_MessageView.color = Color.white;
            m_MessageView.text = "SDK Initialized";
            LoadNextPhoto();
        }

        void OnSDKError(VbErrorType errorType, string errorMessage)
        {
            Debug.LogErrorFormat("OnSDKError {0} ({1})", errorMessage, errorType);
            m_MessageView.enabled = true;
            m_MessageView.color = Color.red;
            m_MessageView.text = errorMessage;
        }

        void OnPhotoLoaded(string photoURI)
        {
            Debug.Log("Photo " + photoURI + " loaded");
            m_MessageView.enabled = false;
            m_IsPhotoLoaded = true;
        }

        void OnPhotoLoadFailed(string photoURI, VbErrorType error)
        {
            Debug.LogErrorFormat("Error Loading Photo ({0})", error);
            m_MessageView.enabled = true;
            m_MessageView.color = Color.red;
            m_MessageView.text = String.Format("Error Loading Photo ({0})", error);
        }

        void LoadNextPhoto()
        {
            m_MessageView.enabled = true;
            m_MessageView.text = "Loading photo";
            m_IsPhotoLoaded = false;
            string uri = EndPoint + m_PhotoIDs[m_CurrentIndex];
            m_PhotoSphere.Render(uri);
            m_CurrentIndex = (m_CurrentIndex + 1) % m_PhotoIDs.Length;
        }

        void SetStereoEnabled(bool enabled)
        {
            if (m_PhotoSphere != null)
            {
                m_PhotoSphere.StereoEnabled = enabled;
            }
        }

        void SwitchVRto2D()
        {
            if (m_PhotoSphere != null)
            {
                m_PhotoSphere.SetVrModeEnabled(false);
                m_PhotoSphere.GyroEnabled = false;
            }
        }

        void Switch2DtoVR()
        {
            if (m_PhotoSphere != null)
            {
                m_PhotoSphere.SetVrModeEnabled(true);
            }
        }
    }
}

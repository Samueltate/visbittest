﻿using UnityEngine;

namespace VB
{
    public class UnityPlugin : UnityPluginInternal
    {
        /// <summary>
        /// Gets the Plugin instance.
        /// </summary>
        /// <returns>The instance.</returns>
        public static UnityPlugin Instance()
        {
            if (m_Instance == null)
            {
                m_Instance = new UnityPlugin();
            }
            return m_Instance as UnityPlugin;
        }

        UnityPlugin()
        {
#if UNITY_ANDROID
            InitForAndroid();
#elif UNITY_IOS
            InitForiOS();
#endif
        }

        public override void ConfigOVRCamera()
        {
#if VB_OCULUS_SDK
            Camera leftCamera = null, rightCamera = null;
            //TODO: This is expensive!
            GameObject cameraRigObj = GameObject.Find("OVRCameraRig");
            if (cameraRigObj == null)
            {
                return;
            }
            OVRCameraRig cameraRig = cameraRigObj.GetComponent<OVRCameraRig>();
            if (cameraRig == null)
            {
                return;
            }
            cameraRig.usePerEyeCameras = true;
            leftCamera = cameraRig.leftEyeCamera;
            rightCamera = cameraRig.rightEyeCamera;
            if (leftCamera != null && rightCamera != null)
            {
                leftCamera.cullingMask &= ~(1 << (int)UnityPluginInternal.Layer.RightLayer);
                rightCamera.cullingMask &= ~(1 << (int)UnityPluginInternal.Layer.LeftLayer);
            }
#endif
        }

        public override void ConfigWVRCamera()
        {
#if VB_VIVE_WAVE_SDK
            Camera leftCamera = null, rightCamera = null;
            //TODO: This is expensive!
            GameObject waveVRObj = GameObject.Find("WaveVR");
            if (waveVRObj == null)
            {
                return;
            }
            WaveVR_Render cameraRender = waveVRObj.GetComponentInChildren<WaveVR_Render>();
            if (cameraRender == null)
            {
                return;
            }
            leftCamera = cameraRender.lefteye.GetCamera();
            rightCamera = cameraRender.righteye.GetCamera();
            if (leftCamera != null && rightCamera != null)
            {
                leftCamera.cullingMask &= ~(1 << (int)UnityPluginInternal.Layer.RightLayer);
                rightCamera.cullingMask &= ~(1 << (int)UnityPluginInternal.Layer.LeftLayer);
            }
#endif
        }
    }
}

﻿#define VISBIT_PLAYERSDK_LEGACY

using System;
using UnityEngine;
using UnityEngine.XR;

namespace Visbit
{
    public class VideoPlayer : MonoBehaviour
    {
        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private Camera leftEyeCamera;

        [SerializeField]
        private Camera rightEyeCamera;

        [SerializeField]
        private Transform headTracker;

        /// <summary>
        /// Determines whether the VideoPlayer restarts from the beginning when it reaches the end of the clip.
        /// </summary>
        public bool isLooping;

        /// <summary>
        /// Whether playback is paused. (Read Only)
        /// </summary>
        /// <value><c>true</c> if is paused; otherwise, <c>false</c>.</value>
        public bool isPaused { get; private set; }

        /// <summary>
        /// Whether content is being played. (Read Only)
        /// </summary>
        /// <value><c>true</c> if is playing; otherwise, <c>false</c>.</value>
        public bool isPlaying { get; private set; }

        /// <summary>
        /// The length of the VideoClip, or the URL, in milliseconds. (Read Only)
        /// </summary>
        /// <value>The length.</value>
        public long length { 
            get
            {
#if VISBIT_PLAYERSDK_LEGACY
                return legacyPlugin.GetVideoDurationMs();
#endif
            }
        }

        /// <summary>
        /// The source that the VideoPlayer uses for playback.
        /// </summary>
        /// <value>The source.</value>
        public VideoSource source { get; private set; }

        /// <summary>
        /// The presentation time of the currently available frame in the unit of millisecond
        /// </summary>
        public long time {
            get
            {
#if VISBIT_PLAYERSDK_LEGACY
                return legacyPlugin.GetCurrentPlaytimeMs();
#endif
            }
        }

        /// <summary>
        /// Errors such as HTTP connection problems are reported through this callback.
        /// </summary>
        public event Action<VisbitError, string> PlayErrorEvent;

        /// <summary>
        /// Invoked when the VideoPlayer preparation is complete.
        /// </summary>
        public event Action VideoIsReadyEvent;

        /// <summary>
        /// Invoked when the video playback reaches its end.
        /// </summary>
        public event Action VideoIsEndedEvent
        {
#if VISBIT_PLAYERSDK_LEGACY
            add { legacyPlugin.VideoIsEndedEvent += value; }
            remove { legacyPlugin.VideoIsEndedEvent -= value; }
#endif
        }
        /// <summary>
        /// Invoked when the video playback is playing or paused
        /// </summary>
        public event Action<bool> VideoIsPlayingEvent;

        /// <summary>
        /// Load the video hosted by the Visbit VR services identified by videoID.
        /// </summary>
        /// <param name="videoID">Video identifier.</param>
        internal void Load(string videoID)
        {
#if VISBIT_PLAYERSDK_LEGACY

#endif
        }

        /// <summary>
        /// Load the videoURI identified by a URI. It can be either a local file or remotely hosted by On-Prem infrastructure.
        /// </summary>
        /// <param name="videoURI">Video URI.</param>
        /// <param name="source">Source.</param>
        public void Load(Uri videoURI, VideoSource source = VideoSource.Offline)
        {
#if VISBIT_PLAYERSDK_LEGACY
            if (source != VideoSource.Offline)
            {
                Debug.LogError("VB_SDK: Only offline playback is supported for now.");
                return;
            }
            var localPath = videoURI.LocalPath;
            Debug.LogFormat("VB_SDK: Load video {0}", localPath);
            legacyPlugin.LoadVideoOffline(localPath, VB.PlayerSettings.OptionAutoPlay);
#endif
        }

        /// <summary>
        /// Starts playback.
        /// </summary>
        public void Play()
        {
#if VISBIT_PLAYERSDK_LEGACY
            legacyPlugin.Play();
#endif
        }

        /// <summary>
        /// Pauses the playback and leaves the current time intact.
        /// </summary>
        public void Pause()
        {
#if VISBIT_PLAYERSDK_LEGACY
            legacyPlugin.Pause();
#endif
        }

        /// <summary>
        /// Seek the player to the specific position.
        /// </summary>
        /// <param name="playerPosition">Player position in the unity of second.</param>
        public void SeekTo(long playerPosition)
        {
#if VISBIT_PLAYERSDK_LEGACY
            legacyPlugin.SeekTo((int)playerPosition);
#endif
        }

        /// <summary>
        /// Stops the playback and sets the current time to 0.
        /// </summary>
        public void Stop()
        {
            Pause();
            SeekTo(0);
        }

#if VISBIT_PLAYERSDK_LEGACY
        private VB.UnityPlugin legacyPlugin;
        private void Awake()
        {
            legacyPlugin = VB.UnityPlugin.Instance();
            legacyPlugin.InitPlayer();
            legacyPlugin.AttachSphere(gameObject, null);
            legacyPlugin.AttachHead(headTracker);

            if (leftEyeCamera == null || rightEyeCamera == null)
            {
                if (XRSettings.loadedDeviceName.Equals("Oculus"))
                {
                    AttachOVRCameras();
                }
                else if (XRSettings.loadedDeviceName.Equals("MockHMD"))
                {
                    AttachWVRCamera();//TODO: Pico
                }
            }
            if (leftEyeCamera != null)
            {
                leftEyeCamera.cullingMask &= ~(1 << (int)VB.UnityPluginInternal.Layer.RightLayer);
            }
            if (rightEyeCamera != null)
            {
                rightEyeCamera.cullingMask &= ~(1 << (int)VB.UnityPluginInternal.Layer.LeftLayer);
            }
        }

        internal void Update()
        {
            legacyPlugin.Update();
            legacyPlugin.UpdateTexture();
        }
#endif

        private void AttachOVRCameras()
        {
#if !VB_DISABLE_OVR
            OVRCameraRig ovrCameraRig = null;
            if (mainCamera != null)
            {
                ovrCameraRig = mainCamera.transform.parent?.GetComponentInChildren<OVRCameraRig>();
            }
            else
            {
                ovrCameraRig = Camera.main?.transform.parent?.GetComponentInChildren<OVRCameraRig>();
            }

            if (ovrCameraRig == null)
            {
                ovrCameraRig = GameObject.Find("OVRCameraRig")?.GetComponentInChildren<OVRCameraRig>();
            }
            if (ovrCameraRig == null)
            {
                ovrCameraRig = FindObjectOfType<OVRCameraRig>();
            }
            if (ovrCameraRig == null)
            {
                throw new Exception("VB_SDK: Unable to Find OVRCameraRig in the scene");
            }

            ovrCameraRig.usePerEyeCameras = true;
            leftEyeCamera = ovrCameraRig.leftEyeCamera;
            rightEyeCamera = ovrCameraRig.rightEyeCamera;
#else
            throw new Exception("VB_SDK: Unsupported VR platfrom: Oculus!");
#endif
        }

        public void AttachWVRCamera()
        {
#if VB_VIVE_WAVE_SDK
            //TODO: This is expensive!
            GameObject waveVRObj = GameObject.Find("WaveVR");
            if (waveVRObj == null)
            {
                throw new Exception("VB_SDK: Unable to Find WaveVR in the scene");
                return;
            }
            WaveVR_Render cameraRender = waveVRObj.GetComponentInChildren<WaveVR_Render>();
            if (cameraRender == null)
            {
                return;
            }
            leftEyeCamera = cameraRender.lefteye.GetCamera();
            rightEyeCamera = cameraRender.righteye.GetCamera();
#endif
        }

    }
}

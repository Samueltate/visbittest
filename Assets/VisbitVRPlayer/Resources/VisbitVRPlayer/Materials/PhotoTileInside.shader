// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "PhotoTileInside" {
Properties {
  _MainTex ("Base (RGB)", 2D) = "white" {}
  _AlphaTex ("Base (RGB)", 2D) = "white" {}
  _Stereo ("Stereo (Cardboard Only)", Float) = 0  // 1 for stereo, 0 for mono
}

SubShader {
  Tags { "RenderType"="Opaque" }
  AlphaToMask On
  LOD 100
  
  Pass {  
    Blend SrcAlpha OneMinusSrcAlpha 
    CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"

      struct appdata_t {
        float4 vertex : POSITION;
        float2 texcoord : TEXCOORD0;
      };

      struct v2f {
        float4 vertex : SV_POSITION;
        half2 texcoord : TEXCOORD0;
        half2 alphaTexcoord : TEXCOORD1;
      };

      sampler2D _MainTex;
      sampler2D _AlphaTex;
      float4 _MainTex_ST;
      float _Stereo;
      
      v2f vert (appdata_t v)
      {
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex); 
        o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
        o.texcoord.y *= 1 - (_Stereo / 2.0f); // *= 0.5 if stereo, *= 1 if mono (no change)
        if(unity_StereoEyeIndex == 0) {
            o.texcoord.y += (_Stereo / 2.0f); // += 0.5 if stereo, += 0 if mono (no change)
        }
        o.texcoord.y = 1 - o.texcoord.y;
        o.alphaTexcoord = v.texcoord;
        return o;
      }
      
      fixed4 frag (v2f i) : SV_Target
      {
        fixed4 alpha = tex2D(_AlphaTex, i.alphaTexcoord);
        fixed4 col = tex2D(_MainTex, i.texcoord);
        col.a = alpha.r;
        return col;
      }
    ENDCG
  }
}
}

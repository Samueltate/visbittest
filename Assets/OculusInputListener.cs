using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OculusInputListener : MonoBehaviour
{
    public bool settingsAvailable;
    public bool settingsTriggered;

    public UnityEvent SettingsSelected;
    public UnityEvent SettingsCanceled;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Input.GetAxis("Cancel"));
        if (settingsAvailable)
        {
            ListenForSettingsChange();
        }
        
    }

    void SetSettingsAvailability(bool isAvailable)
    {
        settingsAvailable = isAvailable;
    }
    public void ToggleSettingsAvailability()
    {
        settingsAvailable = !settingsAvailable;
        //Debug.Log("ToggleCalled");
    }

    public void CloseSettings()
    {
        settingsTriggered = false;
        SettingsCanceled.Invoke();
        settingsAvailable = false;
        Invoke("ToggleSettingsAvailability", 1f);
        //AppManager.Instance.UIManager.laserPointer.SetActive(false);
    }

    void ListenForSettingsChange()
    {
        if (!settingsTriggered)
        {
            //TODO JHPH just a theory - I think the value of 1 is too high, more of a threshold value is needed
            //Can't test just yet
            //Debug.Log("Back button value "+Input.GetAxis("Cancel"));

            if (Input.GetAxis("Cancel") == 1)
            {
                //Debug.Log("Cancel called while settings aren't triggered");
                settingsTriggered = true;
                SettingsSelected.Invoke();
                settingsAvailable = false;
                Invoke("ToggleSettingsAvailability", 1f);
                //AppManager.Instance.UIManager.laserPointer.SetActive(true);

            }
        } else {

            if(Input.GetAxis("Cancel") == 1)
            {

                //Debug.Log("Cancel called while settings ARE triggered");
                settingsTriggered = false;
                SettingsCanceled.Invoke();
                settingsAvailable = false;
                Invoke("ToggleSettingsAvailability", 1f);
                //AppManager.Instance.UIManager.laserPointer.SetActive(false);

            }
        }
    }

    void CoolDownSettings()
    {

    }

    }

